package com.example.smartmovie.ui.activity

import com.example.smartmovie.R
import com.example.smartmovie.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    override fun getLayoutID(): Int = R.layout.activity_main
}