package com.example.smartmovie.utils.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.example.smartmovie.data.model.MovieDetail

class MovieDiffUtils(
    private val oldData: List<MovieDetail>,
    private val newData: List<MovieDetail>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size
    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldMovieDetail = oldData.getOrNull(oldItemPosition)
        val newMovieDetail = newData.getOrNull(newItemPosition)

        return if (oldMovieDetail != null && newMovieDetail != null) {
            oldMovieDetail.id == newMovieDetail.id
        } else false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldMovieDetail = oldData.getOrNull(oldItemPosition)
        val newMovieDetail = newData.getOrNull(newItemPosition)

        return if (oldMovieDetail != null && newMovieDetail != null) {
            oldMovieDetail.id == newMovieDetail.id &&
                    oldMovieDetail.backdropPath == newMovieDetail.posterPath &&
                    oldMovieDetail.genres == newMovieDetail.genres &&
                    oldMovieDetail.overview == newMovieDetail.overview &&
                    oldMovieDetail.posterPath == newMovieDetail.posterPath &&
                    oldMovieDetail.productionCountries == newMovieDetail.productionCountries &&
                    oldMovieDetail.runtime == newMovieDetail.runtime &&
                    oldMovieDetail.releaseDate == newMovieDetail.releaseDate &&
                    oldMovieDetail.title == newMovieDetail.title &&
                    oldMovieDetail.voteAverage == newMovieDetail.voteAverage &&
                    oldMovieDetail.spokenLanguages == newMovieDetail.spokenLanguages
        } else false
    }
}