package com.example.smartmovie.utils




sealed class NetworkResponse<out T : Any> {

    data class Success<T : Any>(val body: T) : NetworkResponse<T>()

    data class ApiError( val code: Int) : NetworkResponse<Nothing>()

    data class NetworkError(val error: Exception) : NetworkResponse<Nothing>()

    data class UnknownError(val error: Throwable?) : NetworkResponse<Nothing>()
}
