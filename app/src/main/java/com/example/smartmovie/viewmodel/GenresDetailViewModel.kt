package com.example.smartmovie.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.example.smartmovie.data.model.Genres
import com.example.smartmovie.data.model.MovieResponseApp
import com.example.smartmovie.repository.MovieRepository
import com.example.smartmovie.utils.Constant
import com.example.smartmovie.utils.Constant.NETWORK_ERROR
import com.example.smartmovie.utils.Constant.SERVER_ERROR
import com.example.smartmovie.utils.Constant.UNKNOWN_ERROR
import com.example.smartmovie.utils.LoadingState
import com.example.smartmovie.utils.NetworkResponse
import com.example.smartmovie.utils.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenresDetailViewModel @Inject constructor(private val movieRepository: MovieRepository) :
    ViewModel() {
    private var jogGetMovie: Job? = null


    private val _genresMovie = MutableLiveData<State<MovieResponseApp>>()
    val genresMovie: LiveData<State<MovieResponseApp>>
        get() = _genresMovie

    private val _genres = MutableLiveData<Genres>()
    val genres: LiveData<Genres>
        get() = _genres

    fun setGenres(genres: Genres) {
        _genres.value = genres
    }

    private var currentPage = Constant.START_PAGE
    private var oldData: MovieResponseApp? = null

    fun getGetMovieFromGenres(isLoadMore: Boolean) {
        val genreId = _genres.value?.id
        if (genreId != null) {
            if (_genres.value != null) {
                jogGetMovie?.cancel()
                jogGetMovie = viewModelScope.launch {
                    val response = if (isLoadMore) {
                        _genresMovie.postValue(State.Loading(LoadingState.LOAD_MORE))
                        movieRepository.getMovieFromGenres(genreId, currentPage)
                    } else {
                        _genresMovie.postValue(State.Loading(LoadingState.ON_LOADING))
                        movieRepository.getMovieFromGenres(genreId, Constant.START_PAGE)
                    }
                    _genresMovie.postValue(handleSearchResponse(response, isLoadMore))
                }
            }
        } else {
            Log.w("GenresDetailViewModel", "#getGetMovieFromGenres genresId is null")
        }
    }

    private fun handleSearchResponse(
        networkResponse: NetworkResponse<MovieResponseApp>,
        isLoadMore: Boolean
    ): State<MovieResponseApp> {
        return when (networkResponse) {
            is NetworkResponse.Success -> {
                currentPage = networkResponse.body.page
                currentPage++
                if (isLoadMore) {
                    oldData?.results?.addAll(networkResponse.body.results)
                } else {
                    oldData = networkResponse.body
                }
                State.Success(oldData)
            }
            is NetworkResponse.NetworkError -> {
                State.Error(NETWORK_ERROR)
            }
            is NetworkResponse.ApiError -> {
                State.Error(SERVER_ERROR)
            }
            is NetworkResponse.UnknownError -> {
                State.Error(UNKNOWN_ERROR)
            }
        }
    }
}