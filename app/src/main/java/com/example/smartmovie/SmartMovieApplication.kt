package com.example.smartmovie

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SmartMovieApplication : Application() {
}