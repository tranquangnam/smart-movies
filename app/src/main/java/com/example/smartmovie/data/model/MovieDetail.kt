package com.example.smartmovie.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetail(

    @SerializedName("backdrop_path") val backdropPath: String?,
    @SerializedName("id") val id: Int?,
    @SerializedName("title") val title: String?,
    @SerializedName("genres") val genres: List<Genres>?,
    @SerializedName("vote_average") val voteAverage: Double?,
    @SerializedName("runtime") val runtime: Int?,
    @SerializedName("release_date") val releaseDate: String?,
    @SerializedName("spoken_languages") val spokenLanguages: List<SpokenLanguages>?,
    @SerializedName("production_countries") val productionCountries: List<ProductionCountries>,
    @SerializedName("overview") val overview: String?,
    @SerializedName("poster_path") val posterPath: String?,
) : Parcelable