package com.example.smartmovie.ui.fragment.home.discover

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.smartmovie.R
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.data.model.MovieResponseApp
import com.example.smartmovie.ui.adapter.MovieAdapter
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.Constant.NOW_PLAYING_NAME
import com.example.smartmovie.utils.Constant.NUMBER_COLUMN_GRID
import com.example.smartmovie.utils.Constant.POPULAR_NAME
import com.example.smartmovie.utils.Constant.TOP_RATED_NAME
import com.example.smartmovie.utils.Constant.TYPE_FRAGMENT
import com.example.smartmovie.utils.Constant.UP_COMING_NAME
import com.example.smartmovie.utils.LoadingState
import com.example.smartmovie.utils.ScrollListener
import com.example.smartmovie.utils.State
import com.example.smartmovie.viewmodel.DiscoverViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OtherFragment : BaseFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_other

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { arguments ->
            typeFragment = arguments.getString(TYPE_FRAGMENT)
        }
    }

    private var typeFragment: String? = null
    private val discoverViewModel: DiscoverViewModel by viewModels(
        ownerProducer = { requireParentFragment() }
    )
    private var isRefresh = false
    private var isLoading = false
    private var isLastPage = false
    private var isScrolling = false

    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var recyclerViewMovies: RecyclerView? = null
    private var progressLoading: ProgressBar? = null
    private var tvNoResult: TextView? = null

    private val movieAdapter = MovieAdapter()
    private var gridLayoutManager: GridLayoutManager? = null
    private var linearLayoutManager: LinearLayoutManager? = null

    override fun onViewReady(view: View) {
        gridLayoutManager = GridLayoutManager(requireContext(), NUMBER_COLUMN_GRID)
        linearLayoutManager = LinearLayoutManager(requireContext())
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        recyclerViewMovies = view.findViewById(R.id.rvResult)
        progressLoading = view.findViewById(R.id.progressLoading)
        tvNoResult = view.findViewById(R.id.tvNoResult)

        initRecyclerview()
        initAdapter()
    }

    private fun initAdapter() {
        movieAdapter.apply {
            setViewType(true)
            onClick = { movieDetail ->
                goToMovieDetailScreen(movieDetail)
            }
        }
    }

    private fun goToMovieDetailScreen(movieDetail: MovieDetail) {
        val action = DiscoverFragmentDirections.actionDiscoverFragmentToMovieDetailFragment3()
        action.movieDetail = movieDetail
        findNavController().navigate(action)
    }

    private fun initRecyclerview() {
        recyclerViewMovies?.apply {
            layoutManager = linearLayoutManager
            adapter = movieAdapter
            addOnScrollListener(this@OtherFragment.scrollListener)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(type: String) = OtherFragment().apply {
            arguments = Bundle().apply {
                putString(TYPE_FRAGMENT, type)
            }
        }
    }

    private fun changeToList() {
        movieAdapter.setViewType(true)
        recyclerViewMovies?.layoutManager = linearLayoutManager
    }

    private fun changeToGrid() {
        movieAdapter.setViewType(false)
        recyclerViewMovies?.layoutManager = gridLayoutManager
    }

    override fun observeLiveData() {
        super.observeLiveData()
        observerData(typeFragment)

        discoverViewModel.viewType.observe(this, { viewType ->
            if (viewType) {
                changeToList()
            } else {
                changeToGrid()
            }
        })
    }

    private fun observerData(type: String?) {
        when (type) {
            POPULAR_NAME -> {
                discoverViewModel.popularMovies.observe(this, { resources ->
                    handleStatus(resources)
                })
            }
            TOP_RATED_NAME -> {
                discoverViewModel.topRatedMovie.observe(this, { resources ->
                    handleStatus(resources)
                })
            }
            NOW_PLAYING_NAME -> {
                discoverViewModel.nowPlayingMovies.observe(this, { resources ->
                    handleStatus(resources)
                })
            }
            UP_COMING_NAME -> {
                discoverViewModel.upComingMovies.observe(this, { resources ->
                    handleStatus(resources)
                })
            }
        }
    }

    private fun handleStatus(state: State<MovieResponseApp>) {
        when (state) {
            is State.Success -> {
                hideProgressLoading()
                if (state.data != null) {
                    if (state.data.results.isNotEmpty()) {
                        movieAdapter.setData(state.data.results)
                        showResult()
                        isLastPage = state.data.page == state.data.totalPages
                    } else {
                        showNoResult()
                    }
                } else {
                    Log.w(typeFragment, "#handleStatus response no data")
                }
            }
            is State.Error -> {
                showDiaLog()
            }
            is State.Loading -> {
                state.stateLoading?.let { loadingState -> showProgressLoading(loadingState) }
            }
        }
    }

    private val clickReload: (() -> Unit) = {
        if (isRefresh) {
            getMovie(false)
        } else {
            getMovie(true)
        }
    }

    private fun showDiaLog() {
        val dialog = showAlertDialog(
            getString(R.string.load_data_failed),
            getString(R.string.message),
            getString(R.string.reload),
            clickReload
        )
        if (dialog?.isShowing == true) return
        dialog?.show()
    }


    private fun showProgressLoading(loadingState: LoadingState) {
        when (loadingState) {
            LoadingState.ON_LOADING -> {
                if (isRefresh) {
                    showRefreshLoading()
                } else {
                    showLoadingCenter()
                }
            }
            LoadingState.LOAD_MORE -> {
                showLoadingBottom()
            }
        }
    }

    private fun showRefreshLoading() {
        swipeRefreshLayout?.isRefreshing = true
    }

    private fun hideProgressLoading() {
        isRefresh = false
        progressLoading?.isVisible = false
        swipeRefreshLayout?.isVisible = true
        swipeRefreshLayout?.isRefreshing = false
    }

    private fun showLoadingCenter() {
        swipeRefreshLayout?.isVisible = false
        progressLoading?.isVisible = true
    }

    private fun showLoadingBottom() {
        swipeRefreshLayout?.isVisible = true
        progressLoading?.isVisible = true
    }


    private fun showResult() {
        recyclerViewMovies?.isVisible = true
        tvNoResult?.isVisible = false
    }

    private fun showNoResult() {
        recyclerViewMovies?.isVisible = false
        tvNoResult?.isVisible = true
    }

    private fun handleRefresh() {
        swipeRefreshLayout?.setOnRefreshListener {
            isRefresh = true
            getMovie(false)
        }
    }

    override fun initAction() {
        super.initAction()
        handleRefresh()
    }

    private fun getMovie(isLoadMore: Boolean) {
        when (typeFragment) {
            POPULAR_NAME -> {
                discoverViewModel.getMoviePopular(isLoadMore)
            }
            TOP_RATED_NAME -> {
                discoverViewModel.getMovieTopRated(isLoadMore)
            }
            UP_COMING_NAME -> {
                discoverViewModel.getMovieUpComing(isLoadMore)
            }
            NOW_PLAYING_NAME -> {
                discoverViewModel.getMovieNowPlaying(isLoadMore)
            }
        }
    }


    private val scrollListener = object : ScrollListener() {
        override fun loading() {
            getMovie(true)
            isScrolling = false
        }

        override var isCheckLoading: Boolean
            get() = isLoading
            set(value) {
                isLoading = value
            }
        override var isCheckLastPage: Boolean
            get() = isLastPage
            set(value) {
                isLastPage = value
            }
        override var isCheckScrolling: Boolean
            get() = isScrolling
            set(value) {
                isScrolling = value
            }
    }

    override fun onDestroyView() {
        recyclerViewMovies?.removeOnScrollListener(scrollListener)
        super.onDestroyView()
    }
}