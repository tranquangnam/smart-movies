package com.example.smartmovie.data.response

import com.example.smartmovie.data.model.Genres
import com.google.gson.annotations.SerializedName

data class GenresResponse(@SerializedName("genres") var genres: List<Genres>?)
