package com.example.smartmovie.ui.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter(fragment: Fragment) :
    FragmentStateAdapter(fragment) {
    private var screens = listOf<Fragment>()

    fun setData(screens: List<Fragment>) {
        this.screens = screens
    }

    override fun getItemCount(): Int = screens.size

    override fun createFragment(position: Int): Fragment {
        return screens[position]
    }
}