package com.example.smartmovie.utils

import android.content.Context
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.smartmovie.R
import com.example.smartmovie.data.model.Genres
import com.example.smartmovie.data.model.SpokenLanguages

object Utils {

    fun getProgressDrawable(context: Context): CircularProgressDrawable {
        return CircularProgressDrawable(context).apply {
            strokeWidth = Constant.PROGRESS_STROKE_WIDTH
            centerRadius = Constant.PROGRESS_CENTER_RADIUS
            start()
        }
    }

    fun loadImage(imageView: ImageView, url: String?, progressDrawable: CircularProgressDrawable) {
        val options = RequestOptions()
            .placeholder(progressDrawable)
            .error(R.drawable.ic_image_not)
        Glide.with(imageView.context)
            .setDefaultRequestOptions(options)
            .load(url)
            .into(imageView)
    }

    fun getLanguage(listString: List<SpokenLanguages>?): String {
        return listString?.joinToString(" , ") { item -> item.englishName.toString() }
            ?: Constant.STRING_EMPTY
    }

    fun getGenresName(genresName: List<Genres>?): String {
        return genresName?.joinToString(" | ") { item -> item.name.toString() }
            ?: Constant.STRING_EMPTY
    }

    fun getRatingValue(value: Double?): Float {
        return value?.div(2)?.toFloat() ?: Constant.DEFAULT_RATING
    }


    fun minutesToHour(minutes: Int?): String {
        return if (minutes != null) {

            val hour = minutes / 60
            val min = minutes % 60
            if (hour > 0) {
                "${hour}h, ${min}min "
            } else {
                "${min}min"
            }
        } else Constant.STRING_EMPTY
    }
}