package com.example.smartmovie.ui.fragment.home.artists


import android.view.View
import com.example.smartmovie.R
import com.example.smartmovie.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArtistsFragment : BaseFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_artists

    override fun onViewReady(view: View) {

    }
}