package com.example.smartmovie.ui.fragment.home.discover

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.example.smartmovie.R
import com.example.smartmovie.data.model.Category
import com.example.smartmovie.ui.adapter.ViewPagerAdapter
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.State
import com.example.smartmovie.viewmodel.DiscoverViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DiscoverFragment : BaseFragment() {
    companion object {
        const val TAG = "DiscoverFragment"
    }

    override fun getLayoutId(): Int = R.layout.fragment_discover
    private val discoverViewModel: DiscoverViewModel by viewModels()

    private var viewPager: ViewPager2? = null
    private var tabLayout: TabLayout? = null
    private var toolbar: Toolbar? = null
    private var processLoading: ProgressBar? = null
    private var isFirstLoad = true

    override fun onViewReady(view: View) {
        viewPager = view.findViewById(R.id.viewPager)
        tabLayout = view.findViewById(R.id.tabLayout)
        toolbar = view.findViewById(R.id.toolBar)
        processLoading = view.findViewById(R.id.progressLoading)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        discoverViewModel.getAllMovies()
    }

    override fun initAction() {
        super.initAction()
        handleChangerViewTypeButton()
    }

    override fun observeLiveData() {
        super.observeLiveData()
        discoverViewModel.category.observe(this, { resource ->
            handleStatus(resource)
        })
    }

    private fun handleStatus(resources: State<List<Category>>) {
        when (resources) {
            is State.Success -> {
                hideProgressLoading()
                if (resources.data != null) {
                    initViePager(resources.data)
                } else {
                    Log.w(TAG, "#observeLiveData response no data")
                }
            }
            is State.Error -> {
                hideProgressLoading()
                showDiaLog()
            }
            is State.Loading -> {
                showProgressLoading()
            }
        }
    }

    private val clickReload: (() -> Unit) = {
        discoverViewModel.getAllMovies()
    }

    private fun showDiaLog() {
        val dialog = showAlertDialog(
            getString(R.string.load_data_failed),
            getString(R.string.message),
            getString(R.string.reload),
            clickReload
        )
        if (dialog?.isShowing == true) return
        dialog?.show()
    }

    private fun showProgressLoading() {
        processLoading?.isVisible = isFirstLoad
    }

    private fun hideProgressLoading() {
        isFirstLoad = false
        processLoading?.isVisible = false
    }

    private fun handleChangerViewTypeButton() {
        toolbar?.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.actionChangeStyle -> {
                    discoverViewModel.changerViewType()
                    //changer icon
                    discoverViewModel.viewType.observe(this, { viewType ->
                        if (viewType) {
                            menuItem.setIcon(R.drawable.ic_list_24)
                        } else {
                            menuItem.setIcon(R.drawable.sharp_grid_view_24)
                        }
                    })
                    true
                }
                else -> false
            }
        }
    }

    private fun initViePager(category: List<Category>) {
        val viewPagerAdapter = ViewPagerAdapter(this)
        val screens = mutableListOf<Fragment>(MoviesFragment())
        category.forEach { itemCategory ->
            screens.add(OtherFragment.newInstance(itemCategory.categoryName))
        }
        viewPagerAdapter.setData(screens)
        viewPager?.adapter = viewPagerAdapter
        if (tabLayout != null && viewPager != null) {
            TabLayoutMediator(tabLayout!!, viewPager!!) { tab, position ->
                when (position) {
                    0 -> tab.text = getString(R.string.movies)
                    else -> {
                        tab.text = category[position - 1].categoryName
                    }
                }
            }.attach()
        }
    }
}