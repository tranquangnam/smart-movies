package com.example.smartmovie.network

import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.data.response.CastResponse
import com.example.smartmovie.data.response.GenresResponse
import com.example.smartmovie.data.response.MovieResponse
import com.example.smartmovie.utils.Constant.GENRES_PARAM
import com.example.smartmovie.utils.Constant.MOVIE_ID
import com.example.smartmovie.utils.Constant.PAGE_PARAM
import com.example.smartmovie.utils.Constant.QUERY_PARAM
import com.example.smartmovie.utils.Constant.URL_CAST
import com.example.smartmovie.utils.Constant.URL_GENRES
import com.example.smartmovie.utils.Constant.URL_GENRES_MOVIE
import com.example.smartmovie.utils.Constant.URL_MOVIE_DETAIL
import com.example.smartmovie.utils.Constant.URL_MOVIE_POPULAR
import com.example.smartmovie.utils.Constant.URL_NOW_PLAYING
import com.example.smartmovie.utils.Constant.URL_SEARCH
import com.example.smartmovie.utils.Constant.URL_TOP_RATED
import com.example.smartmovie.utils.Constant.URL_UP_COMING
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET(URL_SEARCH)
    suspend fun search(
        @Query(QUERY_PARAM) stringQuery: String,
        @Query(PAGE_PARAM) page: Int? = 1
    ): Response<MovieResponse>

    @GET(URL_MOVIE_POPULAR)
    suspend fun getMoviePopular(@Query(PAGE_PARAM) page: Int): Response<MovieResponse>

    @GET(URL_TOP_RATED)
    suspend fun getMovieTopRated(@Query(PAGE_PARAM) page: Int): Response<MovieResponse>

    @GET(URL_UP_COMING)
    suspend fun getMovieUpComing(@Query(PAGE_PARAM) page: Int): Response<MovieResponse>

    @GET(URL_NOW_PLAYING)
    suspend fun getMovieNowPlaying(@Query(PAGE_PARAM) page: Int): Response<MovieResponse>

    @GET(URL_MOVIE_DETAIL)
    suspend fun getMovieDetail(@Path(MOVIE_ID) movieId: Int): Response<MovieDetail>

    @GET(URL_GENRES)
    suspend fun getGenres(): Response<GenresResponse>

    @GET(URL_GENRES_MOVIE)
    suspend fun getMovieFromGenres(
        @Query(GENRES_PARAM) genresId: Int,
        @Query(PAGE_PARAM) page: Int
    ): Response<MovieResponse>


    @GET(URL_CAST)
    suspend fun getCast(@Path(MOVIE_ID) movieId: Int): Response<CastResponse>
}