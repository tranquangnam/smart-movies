package com.example.smartmovie.ui.fragment.home.discover

import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.example.smartmovie.R
import com.example.smartmovie.data.model.Category
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.ui.adapter.CategoryAdapter
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.State
import com.example.smartmovie.viewmodel.DiscoverViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesFragment : BaseFragment() {
    companion object {
        const val TAG = "MoviesFragment"
    }

    override fun getLayoutId(): Int = R.layout.fragment_movies
    private var recyclerViewCategory: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private val categoryAdapter = CategoryAdapter()

    private val discoverViewModel: DiscoverViewModel by viewModels(
        ownerProducer = { requireParentFragment() }
    )

    private var _isRefresh = false
    override fun onViewReady(view: View) {
        recyclerViewCategory = view.findViewById(R.id.rvCategory)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        initRecyclerView()
        initAdapter()
    }

    private fun initAdapter() {
        val viewPagerPrent = parentFragment?.view?.findViewById<ViewPager2>(R.id.viewPager)
        categoryAdapter.apply {
            onClick = { position ->
                viewPagerPrent?.currentItem = position + 1
            }
            onMovieClick = { movieDetail ->
                goToMovieDetailScreen(movieDetail)
            }
        }
    }

    private fun goToMovieDetailScreen(movieDetail: MovieDetail) {
        val action = DiscoverFragmentDirections.actionDiscoverFragmentToMovieDetailFragment3()
        action.movieDetail = movieDetail
        findNavController().navigate(action)
    }

    private fun initRecyclerView() {
        recyclerViewCategory?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = categoryAdapter
        }
    }

    override fun initAction() {
        super.initAction()
        handleRefreshAction()
    }

    private fun handleRefreshAction() {
        swipeRefreshLayout?.setOnRefreshListener {
            _isRefresh = true
            discoverViewModel.getAllMovies()
        }
    }

    private fun handleStatus(state: State<List<Category>>) {
        when (state) {
            is State.Success -> {
                hideProgressLoading()
                if (state.data != null) {
                    categoryAdapter.setData(state.data)
                } else {
                    Log.w(TAG, "#handleStatus response no data")
                }
            }
            is State.Error -> {
                showDiaLog()
            }
            is State.Loading -> {
                showProgressLoading()
            }
        }
    }

    private fun showProgressLoading() {
        swipeRefreshLayout?.isRefreshing = true
    }

    private fun hideProgressLoading() {
        _isRefresh = false
        swipeRefreshLayout?.isRefreshing = false
    }

    private val clickReload: (() -> Unit) = {
        discoverViewModel.getAllMovies()
    }

    private fun showDiaLog() {
        val dialog = showAlertDialog(
            getString(R.string.load_data_failed),
            getString(R.string.message),
            getString(R.string.reload),
            clickReload
        )
        if (dialog?.isShowing == true) return
        dialog?.show()
    }

    override fun observeLiveData() {
        super.observeLiveData()
        discoverViewModel.category.observe(this, { state ->
            handleStatus(state)
        })

        discoverViewModel.viewType.observe(this, { viewType ->
            categoryAdapter.setViewType(viewType)
            recyclerViewCategory?.adapter = categoryAdapter
        })
    }
}