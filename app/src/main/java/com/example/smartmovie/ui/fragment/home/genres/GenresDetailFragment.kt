package com.example.smartmovie.ui.fragment.home.genres

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.smartmovie.R
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.data.model.MovieResponseApp
import com.example.smartmovie.ui.adapter.MovieAdapter
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.LoadingState
import com.example.smartmovie.utils.ScrollListener
import com.example.smartmovie.utils.State
import com.example.smartmovie.viewmodel.GenresDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GenresDetailFragment : BaseFragment() {
    companion object {
        private const val TAG = "GenresDetailFragment"
    }

    private var isLoading = false
    private var isLastPage = false
    private var isScrolling = false
    private var isReFresh = false
    private var isDestroyView = false

    private val args: GenresDetailFragmentArgs by navArgs()
    override fun getLayoutId(): Int = R.layout.fragment_genres_detail
    private val genresDetailViewModel: GenresDetailViewModel by viewModels()

    private var recyclerViewMovie: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var toolbar: Toolbar? = null
    private var processLoading: ProgressBar? = null
    private var textViewNoResult: TextView? = null
    private val movieAdapter = MovieAdapter()

    override fun onViewReady(view: View) {
        recyclerViewMovie = view.findViewById(R.id.rvResult)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        toolbar = view.findViewById(R.id.toolBar)
        processLoading = view.findViewById(R.id.progressLoading)
        textViewNoResult = view.findViewById(R.id.tvNoResult)
        initRecyclerView()
        initAdapter()
    }

    private fun initAdapter() {
        movieAdapter.apply {
            setViewType(true)//list
            onClick = { movieDetail ->
                goToMovieDetail(movieDetail)
            }
        }
    }

    private fun goToMovieDetail(movieDetail: MovieDetail) {
        val action =
            GenresDetailFragmentDirections.actionGenresDetailFragmentToMovieDetailFragment2()
        action.movieDetail = movieDetail
        findNavController().navigate(action)
    }

    private fun initRecyclerView() {
        recyclerViewMovie?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = movieAdapter
            addOnScrollListener(this@GenresDetailFragment.scrollListener)
        }
    }

    private fun receiverGenres() {
        args.let {
            val genres = args.genres
            if (genres != null) {
                genresDetailViewModel.setGenres(genres)
            }
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState == null && !isDestroyView) {
            genresDetailViewModel.getGetMovieFromGenres(false)
        }
    }

    override fun observeLiveData() {
        super.observeLiveData()
        genresDetailViewModel.genresMovie.observe(this, { resources ->
            handleStatus(resources)
        })

        genresDetailViewModel.genres.observe(this, { genres ->
            toolbar?.title = genres.name
        })
    }

    private fun handleStatus(state: State<MovieResponseApp>) {
        when (state) {
            is State.Success -> {
                hideProgressLoading()
                if (state.data != null) {
                    if (state.data.results.isNotEmpty()) {
                        movieAdapter.setData(state.data.results)
                        showResult()
                        isLastPage = state.data.page == state.data.totalPages
                    } else {
                        showNoResult()
                    }
                } else {
                    Log.w(TAG, "#observeLiveData response no data")
                }
            }
            is State.Error -> {
                showDiaLog()
            }
            is State.Loading -> {
                state.stateLoading?.let { loadingState -> showProgressLoading(loadingState) }
            }
        }
    }

    private val clickReload: (() -> Unit) = {
        if (isReFresh) {
            genresDetailViewModel.getGetMovieFromGenres(false)
        } else {
            genresDetailViewModel.getGetMovieFromGenres(true)
        }
    }

    private fun showDiaLog() {
        val dialog = showAlertDialog(
            getString(R.string.load_data_failed),
            getString(R.string.message),
            getString(R.string.reload),
            clickReload
        )
        if (dialog?.isShowing == true) return
        dialog?.show()
    }

    private fun handleRefresh() {
        swipeRefreshLayout?.setOnRefreshListener {
            isReFresh = true
            genresDetailViewModel.getGetMovieFromGenres(false)
        }
    }

    private fun showProgressLoading(loadingState: LoadingState) {
        when (loadingState) {
            LoadingState.ON_LOADING -> {
                if (isReFresh) {
                    showRefreshLoading()
                } else {
                    showLoadingCenter()
                }
            }
            LoadingState.LOAD_MORE -> {
                showLoadingBottom()
            }
        }
    }

    private fun showRefreshLoading() {
        swipeRefreshLayout?.isRefreshing = true
    }

    private fun hideProgressLoading() {
        isReFresh = false
        processLoading?.isVisible = false
        swipeRefreshLayout?.isVisible = true
        swipeRefreshLayout?.isRefreshing = false
    }

    private fun showLoadingCenter() {
        swipeRefreshLayout?.isVisible = false
        processLoading?.isVisible = true
    }

    private fun showLoadingBottom() {
        swipeRefreshLayout?.isVisible = true
        processLoading?.isVisible = true
    }


    private val scrollListener = object : ScrollListener() {
        override fun loading() {
            genresDetailViewModel.getGetMovieFromGenres(true)
            isScrolling = false
        }

        override var isCheckLoading: Boolean
            get() = isLoading
            set(value) {
                isLoading = value
            }
        override var isCheckLastPage: Boolean
            get() = isLastPage
            set(value) {
                isLastPage = value
            }
        override var isCheckScrolling: Boolean
            get() = isScrolling
            set(value) {
                isScrolling = value
            }
    }

    override fun initAction() {
        super.initAction()
        handleBackButton()
        handleRefresh()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        receiverGenres()
    }

    private fun handleBackButton() {
        toolbar?.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }


    override fun onDestroyView() {
        isDestroyView = true
        recyclerViewMovie?.removeOnScrollListener(scrollListener)
        super.onDestroyView()
    }

    private fun showResult() {
        recyclerViewMovie?.isVisible = true
        textViewNoResult?.isVisible = false
    }

    private fun showNoResult() {
        recyclerViewMovie?.isVisible = false
        textViewNoResult?.isVisible = true
    }
}