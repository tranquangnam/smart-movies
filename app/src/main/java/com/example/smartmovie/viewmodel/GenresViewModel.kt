package com.example.smartmovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.smartmovie.data.model.Genres
import com.example.smartmovie.network.ApiConfig
import com.example.smartmovie.repository.MovieRepository
import com.example.smartmovie.utils.Constant.NETWORK_ERROR
import com.example.smartmovie.utils.Constant.SERVER_ERROR
import com.example.smartmovie.utils.Constant.UNKNOWN_ERROR
import com.example.smartmovie.utils.LoadingState
import com.example.smartmovie.utils.NetworkResponse
import com.example.smartmovie.utils.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenresViewModel @Inject constructor(private val movieRepository: MovieRepository) :
    ViewModel() {
    private var jobGetGenres: Job? = null

    private val _genresResponses = MutableLiveData<State<List<Genres>>>()
    val genresResponses: LiveData<State<List<Genres>>>
        get() = _genresResponses

    fun getGenres() {
        jobGetGenres?.cancel()
        jobGetGenres = viewModelScope.launch {
            _genresResponses.postValue(State.Loading(LoadingState.ON_LOADING))
            val response = movieRepository.getGenres()
            _genresResponses.postValue(handleGenresResponse(response))
        }
    }

    private fun handleGenresResponse(networkResponse: NetworkResponse<List<Genres>>): State<List<Genres>> {
        return when (networkResponse) {
            is NetworkResponse.Success -> {
                State.Success(networkResponse.body)
            }
            is NetworkResponse.NetworkError -> {
                State.Error(NETWORK_ERROR)
            }
            is NetworkResponse.ApiError -> {
                State.Error(SERVER_ERROR)
            }
            is NetworkResponse.UnknownError -> {
                State.Error(UNKNOWN_ERROR)
            }
        }
    }

    override fun onCleared() {
        jobGetGenres?.cancel()
        super.onCleared()
    }
}