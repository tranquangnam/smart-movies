package com.example.smartmovie.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmovie.R
import com.example.smartmovie.data.model.Cast
import com.example.smartmovie.utils.Constant
import com.example.smartmovie.utils.Utils
import com.example.smartmovie.utils.Utils.loadImage
import com.example.smartmovie.utils.diffutils.CastDiffUtils

class CastAdapter : RecyclerView.Adapter<CastAdapter.CastViewHolder>() {
    private var casts = mutableListOf<Cast>()

    fun setData(casts: List<Cast>) {
        try {
            val diffUtil = DiffUtil.calculateDiff(CastDiffUtils(this.casts, casts))
            diffUtil.dispatchUpdatesTo(this)

        } catch (ex: Exception) {

        }
        this.casts.clear()
        this.casts.addAll(casts)
    }

    class CastViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val tvName: TextView = itemView.findViewById(R.id.tvName)
        private val imgPoster: ImageView = itemView.findViewById(R.id.imgBackground)
        private val progressDrawable = Utils.getProgressDrawable(itemView.context)

        fun onBind(cast: Cast) {
            tvName.text = cast.name
            if (cast.profilePath != null) {
                loadImage(imgPoster,Constant.URL_IMAGE_CAST + cast.profilePath,progressDrawable )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cast, parent, false)
        return CastViewHolder(view)
    }

    override fun onBindViewHolder(holder: CastViewHolder, position: Int) {
        val cast = casts[position]
        holder.onBind(cast)
    }

    override fun getItemCount(): Int = casts.size
}