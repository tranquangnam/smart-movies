package com.example.smartmovie.ui.base

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    abstract fun getLayoutId(): Int
    abstract fun onViewReady(view: View)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(getLayoutId(), container, false)
        onViewReady(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initAction()
        observeLiveData()
    }

    protected fun hideSoftKeyboard(activity: Activity) {
        if (activity.currentFocus == null) {
            return
        }
        val inputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)
    }

    protected fun showToast(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    protected fun showAlertDialog(
        title: String?, message: String?,
        buttonName: String,
        cb: (() -> Unit)?
    ): Dialog? {
        if (message.isNullOrEmpty()) return null
        val dialog = AlertDialog.Builder(requireContext())
            .setCancelable(false)
            .create()

        if (!title.isNullOrEmpty() && title.isNotBlank()) {
            dialog.setTitle(title)
        }

        dialog.setMessage(message)

        dialog.setButton(AlertDialog.BUTTON_POSITIVE, buttonName) { dialog, _ ->
            dialog.dismiss()
            cb?.invoke()
        }
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }


    open fun initData() = Unit

    open fun initAction() = Unit

    open fun observeLiveData() = Unit

}

