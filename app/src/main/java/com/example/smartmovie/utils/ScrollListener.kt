package com.example.smartmovie.utils

import android.widget.AbsListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class ScrollListener : RecyclerView.OnScrollListener() {
    abstract fun loading()
    abstract var isCheckLoading: Boolean
    abstract var isCheckLastPage: Boolean
    abstract var isCheckScrolling: Boolean

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)

        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
            isCheckScrolling = true
        }
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount

        val isNotLastPageAndNotLoading = !isCheckLoading && !isCheckLastPage
        val isAtLastItem = visibleItemCount + firstVisibleItemPosition >= totalItemCount
        val isNotAtStartItem = firstVisibleItemPosition >= 0

        val isTotalMoreThanVisible = totalItemCount >= Constant.PAGE_SIZE

        val isLoadMore =
            isNotLastPageAndNotLoading && isAtLastItem && isNotAtStartItem && isTotalMoreThanVisible && isCheckScrolling

        if (isLoadMore) {
            loading()
            isCheckScrolling = false
        }
    }
}