package com.example.smartmovie.utils.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.example.smartmovie.data.model.Category

class CategoryDiffUtils(
    private val oldData: List<Category>,
    private val newData: List<Category>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size
    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCategory = oldData.getOrNull(oldItemPosition)
        val newCategory = newData.getOrNull(newItemPosition)

        return if (oldCategory != null && newCategory != null) {
            oldCategory.categoryName == newCategory.categoryName
        } else false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCategory = oldData.getOrNull(oldItemPosition)
        val newCategory = newData.getOrNull(newItemPosition)

        return if (oldCategory != null && newCategory != null) {
            oldCategory.categoryName == newCategory.categoryName && oldCategory.movies == newCategory.movies
        } else false
    }
}