package com.example.smartmovie.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmovie.R
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.utils.Constant
import com.example.smartmovie.utils.Constant.URL_IMAGE_SEARCH
import com.example.smartmovie.utils.Utils
import com.example.smartmovie.utils.Utils.getGenresName
import com.example.smartmovie.utils.Utils.getRatingValue
import com.example.smartmovie.utils.Utils.loadImage
import com.example.smartmovie.utils.diffutils.MovieDiffUtils

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.MovieViewHolder>() {
    var onClick: ((movieDetail: MovieDetail) -> Unit)? = null
    private val movies = mutableListOf<MovieDetail>()
    fun setData(movies: List<MovieDetail>) {
        try {
            val diffUtil = DiffUtil.calculateDiff(MovieDiffUtils(this.movies, movies))
            diffUtil.dispatchUpdatesTo(this)
        } catch (ex: Exception) {

        }
        this.movies.clear()
        this.movies.addAll(movies)
    }


    class MovieViewHolder(itemView: View, onClick: ((movieDetail: MovieDetail) -> Unit)?) :
        RecyclerView.ViewHolder(itemView) {
        private val itemLayout: ConstraintLayout = itemView.findViewById(R.id.layoutItem)
        private val imageBackground: ImageView = itemView.findViewById(R.id.imgBackground)
        private val textViewName: TextView = itemView.findViewById(R.id.tvName)
        private val tvGenresName: TextView = itemView.findViewById(R.id.tvGenresName)
        private val ratingBar: RatingBar = itemView.findViewById(R.id.ratingBar)
        private val progressDrawable = Utils.getProgressDrawable(itemView.context)
        private var movieDetail: MovieDetail? = null

        init {
            itemLayout.setOnClickListener {
                movieDetail?.let { movieDetail ->
                    onClick?.invoke(movieDetail)
                }
            }
        }

        fun onBind(movieDetail: MovieDetail) {
            this.movieDetail = movieDetail
            tvGenresName.text = getGenresName(movieDetail.genres)
            textViewName.text = movieDetail.title
            ratingBar.rating = getRatingValue(movieDetail.voteAverage)
            if (movieDetail.backdropPath != null) {
                loadImage(
                    imageBackground,
                    URL_IMAGE_SEARCH + movieDetail.backdropPath,
                    progressDrawable
                )
            }
        }
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movieDetail = movies[position]
        holder.onBind(movieDetail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false)
        return MovieViewHolder(view, onClick)
    }
}