package com.example.smartmovie.data.response

import com.example.smartmovie.data.model.Cast
import com.google.gson.annotations.SerializedName

data class CastResponse(
    @SerializedName("id") var id: Int?,
    @SerializedName("cast") var cast: List<Cast>?,
)