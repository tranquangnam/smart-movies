package com.example.smartmovie.repository


import com.example.smartmovie.data.model.Cast
import com.example.smartmovie.data.model.Genres
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.data.model.MovieResponseApp
import com.example.smartmovie.utils.NetworkResponse
import com.example.smartmovie.utils.TypeGet

interface IMovieRepository {
    suspend fun querySearch(stringQuery: String, page: Int?): NetworkResponse<MovieResponseApp>
    suspend fun getMovie(page: Int, type: TypeGet): NetworkResponse<MovieResponseApp>
    suspend fun getMovieDetail(movieId: Int): MovieDetail?
    suspend fun getListMovieDetail(movieIds: List<Int?>): List<MovieDetail>
    suspend fun getMovieFromGenres(genresId: Int, page: Int): NetworkResponse<MovieResponseApp>
    suspend fun getGenres(): NetworkResponse<List<Genres>>
    suspend fun getCast(movieId: Int): NetworkResponse<List<Cast>>
}