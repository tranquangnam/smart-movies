package com.example.smartmovie.utils

import com.example.smartmovie.BuildConfig

object Constant {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val QUERY_PARAM = "query"
    const val PAGE_PARAM = "page"
    const val GENRES_PARAM = "with_genre"
    const val MOVIE_ID = "movieId"
    private const val API_KEY = BuildConfig.MOVIE_DB_ACCESS_KEY

    const val URL_SEARCH = "search/movie?api_key=$API_KEY"
    const val URL_MOVIE_POPULAR = "movie/popular?api_key=$API_KEY"
    const val URL_TOP_RATED = "movie/top_rated?api_key=$API_KEY"
    const val URL_UP_COMING = "movie/upcoming?api_key=$API_KEY"
    const val URL_NOW_PLAYING = "movie/now_playing?api_key=$API_KEY"
    const val URL_MOVIE_DETAIL = "movie/{movieId}?api_key=$API_KEY"
    const val URL_CAST = "movie/{movieId}/credits?api_key=$API_KEY"
    const val URL_GENRES = "genre/movie/list?api_key=$API_KEY"
    const val URL_GENRES_MOVIE = "discover/movie?api_key=$API_KEY"


    const val POPULAR_NAME = "Popular"
    const val TOP_RATED_NAME = "Top Rated"
    const val UP_COMING_NAME = "Up Coming"
    const val NOW_PLAYING_NAME = "Now Playing"
    const val TYPE_FRAGMENT = "type_fragment"


    const val URL_IMAGE_SEARCH = "https://image.tmdb.org/t/p/w500"
    const val URL_IMAGE_MOVIE_GRID = "https://image.tmdb.org/t/p/w500"
    const val URL_IMAGE_MOVIE_LIST = "https://image.tmdb.org/t/p/w185"
    const val URL_IMAGE_POSTER = "https://image.tmdb.org/t/p/w185"
    const val URL_IMAGE_CAST = "https://image.tmdb.org/t/p/w185"


    const val STRING_EMPTY = ""
    const val DEFAULT_RATING = 0F
    const val DELAY_SEARCH_TIME = 100L
    const val START_PAGE = 1
    const val PAGE_SIZE = 20

    const val NETWORK_ERROR = "Network Error"
    const val SERVER_ERROR = "Server Error"
    const val UNKNOWN_ERROR = "Unknown Error"

    const val TIME_OUT = 30L

    const val NUMBER_ITEM_IN_CATEGORY = 4
    const val PROGRESS_STROKE_WIDTH = 10f
    const val PROGRESS_CENTER_RADIUS = 50f
    const val NUMBER_COLUMN_GRID = 2

}