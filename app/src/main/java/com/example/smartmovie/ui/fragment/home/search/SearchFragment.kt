package com.example.smartmovie.ui.fragment.home.search

import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmovie.R
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.data.model.MovieResponseApp
import com.example.smartmovie.ui.adapter.SearchAdapter
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.LoadingState
import com.example.smartmovie.utils.ScrollListener
import com.example.smartmovie.utils.State
import com.example.smartmovie.viewmodel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : BaseFragment() {
    companion object {
        private const val TAG = "SearchFragment"
    }

    private var recyclerViewMovieSearch: RecyclerView? = null
    private var textViewCancel: TextView? = null
    private var edtSearch: EditText? = null
    private var textViewNoResult: TextView? = null
    private var toolbar: Toolbar? = null
    private var processLoading: ProgressBar? = null

    private var isLoading = false
    private var isLastPage = false
    private var isScrolling = false

    private val scrollListener = object : ScrollListener() {
        override fun loading() {
            searchViewModel.search(edtSearch?.text.toString(), true)
            isScrolling = false
        }

        override var isCheckLoading: Boolean
            get() = isLoading
            set(value) {
                isLoading = value
            }
        override var isCheckLastPage: Boolean
            get() = isLastPage
            set(value) {
                isLastPage = value
            }
        override var isCheckScrolling: Boolean
            get() = isScrolling
            set(value) {
                isScrolling = value
            }
    }

    private val searchAdapter = SearchAdapter()
    private val searchViewModel: SearchViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.fragment_search

    override fun onViewReady(view: View) {
        recyclerViewMovieSearch = view.findViewById(R.id.rvResult)
        textViewCancel = view.findViewById(R.id.tvCancel)
        edtSearch = view.findViewById(R.id.edtSearch)
        toolbar = view.findViewById(R.id.toolBar)
        textViewNoResult = view.findViewById(R.id.tvNoResult)
        processLoading = view.findViewById(R.id.progressLoading)
        initRecyclerview()
        initAdapter()
    }

    private fun showResult() {
        recyclerViewMovieSearch?.isVisible = true
        textViewNoResult?.isVisible = false
    }

    private fun showNoResult() {
        recyclerViewMovieSearch?.isVisible = false
        textViewNoResult?.isVisible = true
    }


    private fun initRecyclerview() {
        recyclerViewMovieSearch?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = searchAdapter
            addOnScrollListener(this@SearchFragment.scrollListener)
        }
    }

    private fun initAdapter() {
        searchAdapter.onClick = { movieDetail ->
            goToMovieDetailScreen(movieDetail)
        }
    }

    private fun goToMovieDetailScreen(movieDetail: MovieDetail) {
        val action = SearchFragmentDirections.actionSearchFragmentToMovieDetailFragment()
        action.movieDetail = movieDetail
        findNavController().navigate(action)
    }

    override fun observeLiveData() {
        super.observeLiveData()
        searchViewModel.searchResponse.observe(this) { resources ->
            handleStatus(resources)
        }
    }

    private fun handleStatus(state: State<MovieResponseApp>) {
        when (state) {
            is State.Success -> {
                hideProgressLoading()
                if (state.data != null) {
                    if (state.data.results.isNotEmpty()) {
                        searchAdapter.setData(state.data.results)
                        showResult()
                        isLastPage = state.data.page == state.data.totalPages
                    } else {
                        showNoResult()
                    }
                } else {
                    Log.w(TAG, "#observeLiveData response no data")
                }
            }
            is State.Error -> {
                showDiaLog()
            }
            is State.Loading -> {
                state.stateLoading?.let { loadingState -> showProgressLoading(loadingState) }
            }
        }
    }

    private val clickReload: (() -> Unit) = {
        searchViewModel.search(edtSearch?.text.toString(), false)
    }

    private fun showDiaLog() {
        val dialog = showAlertDialog(
            getString(R.string.load_data_failed),
            getString(R.string.message),
            getString(R.string.reload),
            clickReload
        )
        if (dialog?.isShowing == true) return
        dialog?.show()
    }


    private fun showProgressLoading(loadingState: LoadingState) {
        when (loadingState) {
            LoadingState.LOAD_MORE -> {
                showProgressLoadingBottom()
            }
            LoadingState.ON_LOADING -> {
                showProgressLoadingCenter()
            }
        }
    }

    private fun showProgressLoadingBottom() {
        recyclerViewMovieSearch?.isVisible = true
        processLoading?.isVisible = true
    }

    private fun showProgressLoadingCenter() {
        recyclerViewMovieSearch?.isVisible = false
        processLoading?.isVisible = true
    }

    private fun hideProgressLoading() {
        recyclerViewMovieSearch?.isVisible = true
        processLoading?.isVisible = false
    }

    override fun initAction() {
        super.initAction()
        handleSearchButton()
        handleCancelButton()
    }

    private fun handleCancelButton() {
        textViewCancel?.setOnClickListener {
            clearData()
            hideSoftKeyboard(requireActivity())
        }
    }

    private fun clearData() {
        searchViewModel.cancel()
        edtSearch?.text?.clear()
        searchAdapter.setData(listOf())
    }

    private fun handleSearchButton() {
        toolbar?.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.actionSearch -> {
                    searchViewModel.search(edtSearch?.text.toString(), false)
                    hideSoftKeyboard(requireActivity())
                    true
                }
                else -> false
            }
        }
    }

    override fun onDestroyView() {
        recyclerViewMovieSearch?.removeOnScrollListener(this@SearchFragment.scrollListener)
        super.onDestroyView()
    }
}