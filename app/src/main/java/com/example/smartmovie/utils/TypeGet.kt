package com.example.smartmovie.utils

enum class TypeGet {
    POPULAR,
    TOP_RATED,
    UP_COMING,
    NOW_PLAYING,
}