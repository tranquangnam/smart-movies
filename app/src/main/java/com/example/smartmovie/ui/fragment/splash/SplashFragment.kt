package com.example.smartmovie.ui.fragment.splash


import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.smartmovie.R
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.NetworkUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : BaseFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_splash

    override fun onViewReady(view: View) {
    }

    override fun initAction() {
        super.initAction()
        if (NetworkUtils.isNetWorkAvailable(requireContext())) {
            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
        } else {
            showToast("No Internet")
        }
    }
}