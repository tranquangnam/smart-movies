package com.example.smartmovie.utils

sealed class State<T>(
    val data: T? = null,
    val message: String? = null,
    val stateLoading: LoadingState? = null
) {
    class Success<T>(data: T?) : State<T>(data)
    class Error<T>(message: String, data: T? = null) : State<T>(data, message)
    class Loading<T>(stateLoading: LoadingState, data: T? = null) : State<T>(data, null, stateLoading)
}