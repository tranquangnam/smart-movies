package com.example.smartmovie.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmovie.R
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.utils.Constant.URL_IMAGE_MOVIE_GRID
import com.example.smartmovie.utils.Constant.URL_IMAGE_MOVIE_LIST
import com.example.smartmovie.utils.Utils
import com.example.smartmovie.utils.Utils.loadImage
import com.example.smartmovie.utils.Utils.minutesToHour
import com.example.smartmovie.utils.diffutils.MovieDiffUtils

class MovieAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val VIEW_TYPE_LIST = 0
        const val VIEW_TYPE_GRID = 1
    }

    var onClick: ((movieDetail: MovieDetail) -> Unit)? = null

    private var isList = false // true List, false Grid
    fun setViewType(value: Boolean) {
        isList = value
    }

    private val movies = mutableListOf<MovieDetail>()
    fun setData(movies: List<MovieDetail>) {
        try {
            val diffUtil = DiffUtil.calculateDiff(MovieDiffUtils(this.movies, movies))
            diffUtil.dispatchUpdatesTo(this)
        } catch (ex: Exception) {

        }
        this.movies.clear()
        this.movies.addAll(movies)
    }

    class MovieListViewHolder(itemView: View, onClick: ((movieDetail: MovieDetail) -> Unit)?) :
        RecyclerView.ViewHolder(itemView) {
        private val itemLayout: ConstraintLayout = itemView.findViewById(R.id.itemLayout)
        private val imagePoster: ImageView = itemView.findViewById(R.id.imgPoster)
        private val tvName: TextView = itemView.findViewById(R.id.tvName)
        private val tvOverview: TextView = itemView.findViewById(R.id.tvOverview)
        private val tvTime: TextView = itemView.findViewById(R.id.tvTime)
        private val progressDrawable = Utils.getProgressDrawable(itemView.context)
        private var movieDetail: MovieDetail? = null

        init {
            itemLayout.setOnClickListener {
                movieDetail?.let { movieDetail ->
                    onClick?.invoke(movieDetail)
                }
            }
        }

        fun onBind(movieDetail: MovieDetail) {
            this.movieDetail = movieDetail
            tvName.text = movieDetail.title
            tvOverview.text = movieDetail.overview
            tvTime.text = minutesToHour(movieDetail.runtime)

            if (movieDetail.posterPath != null) {
                loadImage(
                    imagePoster,
                    URL_IMAGE_MOVIE_LIST + movieDetail.posterPath,
                    progressDrawable
                )
            }
        }
    }

    class MovieGridViewHolder(itemView: View, onClick: ((movieDetail: MovieDetail) -> Unit)?) :
        RecyclerView.ViewHolder(itemView) {
        private val itemLayout: RelativeLayout = itemView.findViewById(R.id.itemLayout)
        private val imagePoster: ImageView = itemView.findViewById(R.id.imgPoster)
        private val tvName: TextView = itemView.findViewById(R.id.tvName)
        private val tvTime: TextView = itemView.findViewById(R.id.tvTime)
        private val progressDrawable = Utils.getProgressDrawable(itemView.context)
        private var movieDetail: MovieDetail? = null

        init {
            itemLayout.setOnClickListener {
                movieDetail?.let { movieDetail ->
                    onClick?.invoke(movieDetail)
                }
            }
        }

        fun onBind(movieDetail: MovieDetail) {
            this.movieDetail = movieDetail
            tvName.text = movieDetail.title
            tvTime.text = minutesToHour(movieDetail.runtime)

            if (movieDetail.posterPath != null) {
                loadImage(
                    imagePoster,
                    URL_IMAGE_MOVIE_GRID + movieDetail.posterPath,
                    progressDrawable
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_LIST) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_movie_list, parent, false)
            MovieListViewHolder(view, onClick)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_movie_grid, parent, false)
            MovieGridViewHolder(view, onClick)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isList) {
            VIEW_TYPE_LIST
        } else VIEW_TYPE_GRID
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movie = movies[position]
        when (holder) {
            is MovieGridViewHolder -> holder.onBind(movie)
            is MovieListViewHolder -> holder.onBind(movie)
        }
    }

    override fun getItemCount(): Int = movies.size
}