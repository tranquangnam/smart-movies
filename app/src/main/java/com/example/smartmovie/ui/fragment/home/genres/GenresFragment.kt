package com.example.smartmovie.ui.fragment.home.genres

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmovie.R
import com.example.smartmovie.data.model.Genres
import com.example.smartmovie.ui.adapter.GenresAdapter
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.State
import com.example.smartmovie.viewmodel.GenresViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GenresFragment : BaseFragment() {
    companion object {
        private const val TAG = "GenresFragment"
    }

    override fun getLayoutId(): Int = R.layout.fragment_genres

    private var isDestroyView = false
    private var recyclerViewMovieGenres: RecyclerView? = null
    private var toolbar: Toolbar? = null
    private var processLoading: ProgressBar? = null
    private var textViewNoResult: TextView? = null
    private val genresAdapter = GenresAdapter()
    private val genresViewModel: GenresViewModel by viewModels()

    override fun onViewReady(view: View) {
        recyclerViewMovieGenres = view.findViewById(R.id.rvResult)
        processLoading = view.findViewById(R.id.progressLoading)
        textViewNoResult = view.findViewById(R.id.tvNoResult)
        toolbar = view.findViewById(R.id.toolBar)
        initRecyclerview()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState == null && !isDestroyView) {
            genresViewModel.getGenres()
        }
    }

    private fun initRecyclerview() {
        recyclerViewMovieGenres?.apply {
            adapter = genresAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        genresAdapter.onClick = { genres ->
            goToGenresDetailScreen(genres)
        }
    }

    private fun goToGenresDetailScreen(genres: Genres) {
        val action = GenresFragmentDirections.actionGenresFragmentToGenresDetailFragment()
        action.genres = genres
        findNavController().navigate(action)
    }

    override fun observeLiveData() {
        super.observeLiveData()
        genresViewModel.genresResponses.observe(this, { resources ->
            handleStatus(resources)
        })
    }

    private fun handleStatus(state: State<List<Genres>>) {
        when (state) {
            is State.Success -> {
                hideProgressLoading()
                if (state.data != null) {
                    if (state.data.isNotEmpty()) {
                        genresAdapter.setData(state.data)
                        showResult()
                    } else {
                        showNoResult()
                    }
                } else {
                    Log.w(TAG, "#observeLiveData response no data")
                }
            }
            is State.Error -> {
                hideProgressLoading()
                showToast(state.message)
            }
            is State.Loading -> {
                showProgressLoading()
            }
        }
    }

    private fun showProgressLoading() {
        processLoading?.isVisible = true
    }

    private fun hideProgressLoading() {
        processLoading?.isVisible = false
    }

    private fun showResult() {
        recyclerViewMovieGenres?.isVisible = true
        textViewNoResult?.isVisible = false
    }

    private fun showNoResult() {
        recyclerViewMovieGenres?.isVisible = false
        textViewNoResult?.isVisible = true
    }
}