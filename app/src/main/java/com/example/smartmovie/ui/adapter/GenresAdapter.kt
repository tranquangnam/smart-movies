package com.example.smartmovie.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmovie.R
import com.example.smartmovie.data.model.Genres
import com.example.smartmovie.utils.diffutils.GenresDiffUtils

class GenresAdapter : RecyclerView.Adapter<GenresAdapter.GenresViewHolder>() {
    private var genres = mutableListOf<Genres>()

    var onClick: ((genres: Genres) -> Unit)? = null

    fun setData(genres: List<Genres>) {
        try {
            val diffUtil =
                DiffUtil.calculateDiff(GenresDiffUtils(this.genres, genres))
            diffUtil.dispatchUpdatesTo(this)
        } catch (ex: Exception) {

        }
        this.genres.clear()
        this.genres.addAll(genres)
    }

    class GenresViewHolder(view: View, onClick: ((genres: Genres) -> Unit)?) :
        RecyclerView.ViewHolder(view) {
        private var genres: Genres? = null
        private val layoutItem: CardView = view.findViewById(R.id.layoutItem)
        private val tvName: TextView = view.findViewById(R.id.tvGenresName)
        // private val imgBackground: ImageView = view.findViewById(R.id.imgBackground)

        init {
            layoutItem.setOnClickListener {
                genres?.let { genres ->
                    onClick?.invoke(genres)
                }
            }
        }

        fun onBind(genres: Genres) {
            this.genres = genres
            tvName.text = genres.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenresViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_genres, parent, false)
        return GenresViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: GenresViewHolder, position: Int) {
        val genres = genres[position]
        holder.onBind(genres)
    }

    override fun getItemCount(): Int = genres.size
}