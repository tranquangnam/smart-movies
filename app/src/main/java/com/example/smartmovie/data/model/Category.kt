package com.example.smartmovie.data.model

data class Category( val categoryName: String,  val movies: List<MovieDetail>)