package com.example.smartmovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.smartmovie.data.model.Cast
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.network.ApiConfig
import com.example.smartmovie.repository.MovieRepository
import com.example.smartmovie.utils.Constant
import com.example.smartmovie.utils.LoadingState
import com.example.smartmovie.utils.NetworkResponse
import com.example.smartmovie.utils.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(private val movieRepository: MovieRepository) :
    ViewModel() {
    private var jobGetCast: Job? = null
    private val _castResponses = MutableLiveData<State<List<Cast>>>()
    val castResponses: LiveData<State<List<Cast>>>
        get() = _castResponses

    private val _movieDetail = MutableLiveData<MovieDetail>()
    val movieDetail: LiveData<MovieDetail>
        get() = _movieDetail

    fun setMovieDetail(movieDetail: MovieDetail) {
        _movieDetail.postValue(movieDetail)
    }

    fun getCast(movieId: Int?) {
        if (movieId != null) {
            jobGetCast?.cancel()
            jobGetCast = viewModelScope.launch {
                _castResponses.postValue(State.Loading(LoadingState.ON_LOADING))
                val response = movieRepository.getCast(movieId)
                _castResponses.postValue(handleCastResponse(response))
            }
        }
    }

    private fun handleCastResponse(networkResponse: NetworkResponse<List<Cast>>): State<List<Cast>> {
        return when (networkResponse) {
            is NetworkResponse.Success -> {
                State.Success(networkResponse.body)
            }
            is NetworkResponse.NetworkError -> {
                State.Error(Constant.NETWORK_ERROR)
            }
            is NetworkResponse.ApiError -> {
                State.Error(Constant.SERVER_ERROR)
            }
            is NetworkResponse.UnknownError -> {
                State.Error(Constant.UNKNOWN_ERROR)
            }
        }
    }

    override fun onCleared() {
        jobGetCast?.cancel()
        super.onCleared()
    }
}