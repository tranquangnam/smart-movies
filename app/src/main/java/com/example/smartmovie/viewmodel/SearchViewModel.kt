package com.example.smartmovie.viewmodel

import androidx.lifecycle.*
import com.example.smartmovie.data.model.MovieResponseApp
import com.example.smartmovie.network.ApiConfig
import com.example.smartmovie.repository.MovieRepository
import com.example.smartmovie.utils.Constant
import com.example.smartmovie.utils.LoadingState
import com.example.smartmovie.utils.NetworkResponse
import com.example.smartmovie.utils.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val movieRepository: MovieRepository) :
    ViewModel() {
    private var jobSearch: Job? = null

    private val _searchResponse = MutableLiveData<State<MovieResponseApp>>()
    val searchResponse: LiveData<State<MovieResponseApp>>
        get() = _searchResponse

    private var currentPage = Constant.START_PAGE
    private var oldData: MovieResponseApp? = null

    fun search(queryString: String, isLoadMore: Boolean) {
        jobSearch?.cancel()
        jobSearch = viewModelScope.launch {
            if (isLoadMore) {
                _searchResponse.postValue(State.Loading(LoadingState.LOAD_MORE))
                val response = movieRepository.querySearch(queryString, currentPage)
                _searchResponse.postValue(handleSearchResponse(response, true))
            } else {
                _searchResponse.postValue(State.Loading(LoadingState.ON_LOADING))
                val response = movieRepository.querySearch(queryString, Constant.START_PAGE)
                _searchResponse.postValue(handleSearchResponse(response, false))
            }
        }
    }

    private fun handleSearchResponse(
        networkResponse: NetworkResponse<MovieResponseApp>,
        isLoadMore: Boolean
    ): State<MovieResponseApp> {
        return when (networkResponse) {
            is NetworkResponse.Success -> {
                currentPage = networkResponse.body.page
                currentPage++
                if (isLoadMore) {
                    if (oldData != null) {
                        oldData?.results?.addAll(networkResponse.body.results)
                    }
                } else {
                    oldData = networkResponse.body
                }
                State.Success(oldData)
            }
            is NetworkResponse.NetworkError -> {
                State.Error(Constant.NETWORK_ERROR)
            }
            is NetworkResponse.ApiError -> {
                State.Error(Constant.SERVER_ERROR)
            }
            is NetworkResponse.UnknownError -> {
                State.Error(Constant.UNKNOWN_ERROR)
            }
        }
    }

    fun cancel() {
        jobSearch?.cancel()
    }
}
