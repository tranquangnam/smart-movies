package com.example.smartmovie.data.model

data class MovieResponseApp(
    val name: String,
    val page: Int,
    val results: MutableList<MovieDetail>,
    val totalPages: Int,
)
