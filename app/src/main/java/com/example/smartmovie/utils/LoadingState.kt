package com.example.smartmovie.utils


enum class LoadingState {
    ON_LOADING,
    LOAD_MORE
}
