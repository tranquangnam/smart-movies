package com.example.smartmovie.viewmodel

import androidx.lifecycle.*
import com.example.smartmovie.data.model.Category
import com.example.smartmovie.data.model.MovieResponseApp
import com.example.smartmovie.repository.MovieRepository
import com.example.smartmovie.utils.*
import com.example.smartmovie.utils.Constant.NETWORK_ERROR
import com.example.smartmovie.utils.Constant.NUMBER_ITEM_IN_CATEGORY
import com.example.smartmovie.utils.Constant.SERVER_ERROR
import com.example.smartmovie.utils.Constant.START_PAGE
import com.example.smartmovie.utils.Constant.UNKNOWN_ERROR
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DiscoverViewModel @Inject constructor(private val movieRepository: MovieRepository) :
    ViewModel() {
    private var value = true
    private var jobGetAllMovie: Job? = null
    private var jobGetPopular: Job? = null
    private var jobGetTopRated: Job? = null
    private var jobGetNowPlaying: Job? = null
    private var jobGetUpComing: Job? = null
    private val _viewType = MutableLiveData<Boolean>()
    val viewType: LiveData<Boolean>
        get() = _viewType

    fun changerViewType() { //true = List, false = Grid
        value = !value
        _viewType.postValue(value)
    }

    private var currentPagePopular = START_PAGE
    private var oldDataPoPopular: MovieResponseApp? = null

    private var currentPageTopRated = START_PAGE
    private var oldDataTopRated: MovieResponseApp? = null

    private var currentPageUpComing = START_PAGE
    private var oldDataUpComing: MovieResponseApp? = null

    private var currentPageNowPlaying = START_PAGE
    private var oldDataNowPlaying: MovieResponseApp? = null

    private val _category = MutableLiveData<State<List<Category>>>()
    val category: LiveData<State<List<Category>>>
        get() = _category

    private val _popularMovies = MutableLiveData<State<MovieResponseApp>>()
    val popularMovies: LiveData<State<MovieResponseApp>>
        get() = _popularMovies

    private val _topRatedMovies = MutableLiveData<State<MovieResponseApp>>()
    val topRatedMovie: LiveData<State<MovieResponseApp>>
        get() = _topRatedMovies

    private val _upComingMovies = MutableLiveData<State<MovieResponseApp>>()
    val upComingMovies: LiveData<State<MovieResponseApp>>
        get() = _upComingMovies

    private val _nowPlayingMovies = MutableLiveData<State<MovieResponseApp>>()
    val nowPlayingMovies: LiveData<State<MovieResponseApp>>
        get() = _nowPlayingMovies

    fun getAllMovies() {
        jobGetAllMovie?.cancel()
        jobGetAllMovie = viewModelScope.launch {
            _category.postValue(State.Loading(LoadingState.ON_LOADING))
            val response = mutableListOf<NetworkResponse<MovieResponseApp>>()
            coroutineScope {
                launch {
                    val responsePopular = getResponse(TypeGet.POPULAR, START_PAGE)
                    _popularMovies.postValue(
                        handleResponse(responsePopular, TypeGet.POPULAR, false)
                    )
                    response.add(responsePopular)
                }

                launch {
                    val responseTopRated = getResponse(TypeGet.TOP_RATED, START_PAGE)
                    _topRatedMovies.postValue(
                        handleResponse(responseTopRated, TypeGet.TOP_RATED, false)
                    )
                    response.add(responseTopRated)
                }
                launch {
                    val responseUpComing = getResponse(TypeGet.UP_COMING, START_PAGE)
                    _upComingMovies.postValue(
                        handleResponse(responseUpComing, TypeGet.UP_COMING, false)
                    )
                    response.add(responseUpComing)
                }

                launch {
                    val responseNowPlaying = getResponse(TypeGet.NOW_PLAYING, START_PAGE)
                    _nowPlayingMovies.postValue(
                        handleResponse(responseNowPlaying, TypeGet.NOW_PLAYING, false)
                    )
                    response.add(responseNowPlaying)
                }
            }
            _category.postValue(
                handleResponseCategory(response)
            )
        }
    }

    private fun handleResponseCategory(networkResponse: List<NetworkResponse<MovieResponseApp>>): State<List<Category>> {
        val category = mutableListOf<Category>()
        networkResponse.forEach { networkResponseItem ->
            if (networkResponseItem is NetworkResponse.Success) {
                val totalMovies = networkResponseItem.body.results
                val nameType = networkResponseItem.body.name
                val movies =
                    if (totalMovies.size > NUMBER_ITEM_IN_CATEGORY) totalMovies.take(
                        NUMBER_ITEM_IN_CATEGORY
                    ) else totalMovies
                category.add(Category(categoryName = nameType, movies = movies))
            }
        }
        return if (category.isNotEmpty()) {
            State.Success(category)
        } else {
            State.Error(NETWORK_ERROR)
        }
    }

    private suspend fun getResponse(
        typeGet: TypeGet,
        page: Int
    ): NetworkResponse<MovieResponseApp> {
        return when (typeGet) {
            TypeGet.POPULAR -> movieRepository.getMovie(page, TypeGet.POPULAR)
            TypeGet.TOP_RATED -> movieRepository.getMovie(page, TypeGet.TOP_RATED)
            TypeGet.UP_COMING -> movieRepository.getMovie(page, TypeGet.UP_COMING)
            TypeGet.NOW_PLAYING -> movieRepository.getMovie(page, TypeGet.NOW_PLAYING)
        }
    }

    fun getMoviePopular(isLoadMore: Boolean) {
        jobGetNowPlaying?.cancel()
        jobGetPopular = viewModelScope.launch {
            val response = if (isLoadMore) {
                _popularMovies.postValue(State.Loading(LoadingState.LOAD_MORE))
                getResponse(TypeGet.POPULAR, currentPagePopular)
            } else {
                _popularMovies.postValue(State.Loading(LoadingState.ON_LOADING))
                getResponse(TypeGet.POPULAR, START_PAGE)
            }
            _popularMovies.postValue(handleResponse(response, TypeGet.POPULAR, isLoadMore))
        }
    }

    fun getMovieTopRated(isLoadMore: Boolean) {
        jobGetTopRated?.cancel()
        jobGetTopRated = viewModelScope.launch {
            val response = if (isLoadMore) {
                _topRatedMovies.postValue(State.Loading(LoadingState.LOAD_MORE))
                getResponse(TypeGet.TOP_RATED, currentPageTopRated)
            } else {
                _topRatedMovies.postValue(State.Loading(LoadingState.ON_LOADING))
                getResponse(TypeGet.TOP_RATED, START_PAGE)
            }
            _topRatedMovies.postValue(handleResponse(response, TypeGet.TOP_RATED, isLoadMore))
        }
    }

    fun getMovieUpComing(isLoadMore: Boolean) {
        jobGetUpComing?.cancel()
        jobGetUpComing = viewModelScope.launch {
            val response = if (isLoadMore) {
                _upComingMovies.postValue(State.Loading(LoadingState.LOAD_MORE))
                getResponse(TypeGet.UP_COMING, currentPageUpComing)
            } else {
                _upComingMovies.postValue(State.Loading(LoadingState.ON_LOADING))
                getResponse(TypeGet.UP_COMING, START_PAGE)
            }
            _upComingMovies.postValue(handleResponse(response, TypeGet.UP_COMING, isLoadMore))
        }
    }

    fun getMovieNowPlaying(isLoadMore: Boolean) {
        jobGetNowPlaying?.cancel()
        jobGetNowPlaying = viewModelScope.launch {

            val response = if (isLoadMore) {
                _nowPlayingMovies.postValue(State.Loading(LoadingState.LOAD_MORE))
                getResponse(TypeGet.NOW_PLAYING, currentPageNowPlaying)
            } else {
                _nowPlayingMovies.postValue(State.Loading(LoadingState.ON_LOADING))
                getResponse(TypeGet.NOW_PLAYING, START_PAGE)
            }
            _nowPlayingMovies.postValue(handleResponse(response, TypeGet.NOW_PLAYING, isLoadMore))
        }
    }

    private fun addOldData(
        newData: MovieResponseApp,
        typeGet: TypeGet,
        isLoadMore: Boolean
    ): MovieResponseApp? {
        return when (typeGet) {
            TypeGet.POPULAR -> {
                if (isLoadMore) {
                    oldDataPoPopular?.results?.addAll(newData.results)
                } else {
                    oldDataPoPopular = newData
                }
                oldDataPoPopular
            }

            TypeGet.UP_COMING -> {
                if (isLoadMore) {
                    oldDataUpComing?.results?.addAll(newData.results)
                } else {
                    oldDataUpComing = newData
                }
                oldDataUpComing
            }

            TypeGet.TOP_RATED -> {
                if (isLoadMore) {
                    oldDataTopRated?.results?.addAll(newData.results)
                } else {
                    oldDataTopRated = newData
                }
                oldDataTopRated
            }

            TypeGet.NOW_PLAYING -> {
                if (isLoadMore) {
                    oldDataNowPlaying?.results?.addAll(newData.results)
                } else {
                    oldDataNowPlaying = newData
                }
                oldDataNowPlaying
            }
        }
    }

    private fun handleResponse(
        networkResponse: NetworkResponse<MovieResponseApp>,
        typeGet: TypeGet,
        isLoadMore: Boolean
    ): State<MovieResponseApp> {
        return when (networkResponse) {
            is NetworkResponse.Success -> {
                when (typeGet) {
                    TypeGet.POPULAR -> {
                        currentPagePopular = networkResponse.body.page
                        currentPagePopular++
                        State.Success(addOldData(networkResponse.body, TypeGet.POPULAR, isLoadMore))
                    }
                    TypeGet.TOP_RATED -> {
                        currentPageTopRated = networkResponse.body.page
                        currentPageTopRated++
                        State.Success(
                            addOldData(networkResponse.body, TypeGet.TOP_RATED, isLoadMore)
                        )
                    }
                    TypeGet.NOW_PLAYING -> {
                        currentPageNowPlaying = networkResponse.body.page
                        currentPageNowPlaying++
                        State.Success(
                            addOldData(networkResponse.body, TypeGet.NOW_PLAYING, isLoadMore)
                        )
                    }
                    TypeGet.UP_COMING -> {
                        currentPageUpComing = networkResponse.body.page
                        currentPageUpComing++
                        State.Success(
                            addOldData(networkResponse.body, TypeGet.UP_COMING, isLoadMore)
                        )
                    }
                }
            }
            is NetworkResponse.NetworkError -> {
                State.Error(NETWORK_ERROR)
            }
            is NetworkResponse.ApiError -> {
                State.Error(SERVER_ERROR)
            }
            is NetworkResponse.UnknownError -> {
                State.Error(UNKNOWN_ERROR)
            }
        }
    }

    override fun onCleared() {
        jobGetAllMovie?.cancel()
        jobGetPopular?.cancel()
        jobGetTopRated?.cancel()
        jobGetNowPlaying?.cancel()
        jobGetUpComing?.cancel()
        super.onCleared()
    }
}