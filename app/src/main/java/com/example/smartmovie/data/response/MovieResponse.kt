package com.example.smartmovie.data.response

import com.example.smartmovie.data.model.MovieDetail
import com.google.gson.annotations.SerializedName

data class MovieResponse(
    @SerializedName("page") val page: Int?,
    @SerializedName("results") val results: List<MovieDetail>?,
    @SerializedName("total_pages") val totalPages: Int?,
    @SerializedName("total_results") val totalResults: Int?,
)