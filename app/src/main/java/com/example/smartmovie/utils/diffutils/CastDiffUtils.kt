package com.example.smartmovie.utils.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.example.smartmovie.data.model.Cast

class CastDiffUtils(private val oldData: List<Cast>, private val newData: List<Cast>) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size
    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCast = oldData.getOrNull(oldItemPosition)
        val newCast = newData.getOrNull(newItemPosition)

        return if (oldCast != null && newCast != null) {
            oldCast.id == newCast.id
        } else false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCast = oldData.getOrNull(oldItemPosition)
        val newCast = newData.getOrNull(newItemPosition)

        return if (oldCast != null && newCast != null) {
            (oldCast.id == newCast.id
                    && oldCast.name == newCast.name
                    && oldCast.profilePath == newCast.profilePath)
        } else false
    }
}