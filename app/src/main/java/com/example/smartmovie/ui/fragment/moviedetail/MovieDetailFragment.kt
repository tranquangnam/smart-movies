package com.example.smartmovie.ui.fragment.moviedetail

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.example.smartmovie.R
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.ui.adapter.CastAdapter
import com.example.smartmovie.ui.base.BaseFragment
import com.example.smartmovie.utils.Constant
import com.example.smartmovie.utils.Constant.DEFAULT_RATING
import com.example.smartmovie.utils.State
import com.example.smartmovie.utils.Utils
import com.example.smartmovie.utils.Utils.getGenresName
import com.example.smartmovie.utils.Utils.getLanguage
import com.example.smartmovie.utils.Utils.getRatingValue
import com.example.smartmovie.utils.Utils.loadImage
import com.example.smartmovie.utils.Utils.minutesToHour
import com.example.smartmovie.viewmodel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailFragment : BaseFragment() {
    companion object {
        private const val TAG = "MovieDetailFragment"
    }

    private val args: MovieDetailFragmentArgs by navArgs()

    private var imageViewPoster: ImageView? = null
    private var textViewName: TextView? = null
    private var textViewGenres: TextView? = null
    private var ratingBar: RatingBar? = null
    private var textViewRating: TextView? = null
    private var textViewLanguages: TextView? = null
    private var textViewTime: TextView? = null
    private var textViewOverview: TextView? = null
    private var toolbar: Toolbar? = null
    private var recyclerViewCast: RecyclerView? = null
    private var progressLoading: ProgressBar? = null
    private var textViewNoResult: TextView? = null
    private val castAdapter = CastAdapter()
    private var progressDrawable: CircularProgressDrawable? = null

    private val movieDetailViewModel: MovieDetailViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.fragment_movie_detail

    override fun onViewReady(view: View) {
        imageViewPoster = view.findViewById(R.id.imgBackground)
        textViewName = view.findViewById(R.id.tvName)
        textViewGenres = view.findViewById(R.id.tvGenresName)
        ratingBar = view.findViewById(R.id.ratingBar)
        textViewRating = view.findViewById(R.id.tvRate)
        textViewLanguages = view.findViewById(R.id.tvLanguage)
        textViewTime = view.findViewById(R.id.tvTime)
        textViewOverview = view.findViewById(R.id.tvOverview)
        recyclerViewCast = view.findViewById(R.id.rvCast)
        toolbar = view.findViewById(R.id.toolBar)
        textViewNoResult = view.findViewById(R.id.tvNoResult)
        progressLoading = view.findViewById(R.id.progressLoading)
        progressDrawable = Utils.getProgressDrawable(view.context)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState == null) {
            //is first load
            getMovieDetail()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerview()
    }

    private fun initRecyclerview() {
        recyclerViewCast?.apply {
            layoutManager =
                GridLayoutManager(
                    requireContext(),
                    Constant.NUMBER_COLUMN_GRID,
                    RecyclerView.HORIZONTAL,
                    false
                )
            adapter = castAdapter
        }
    }

    override fun initAction() {
        super.initAction()
        handleBackButton()
    }

    private fun showMovieDetail(movieDetail: MovieDetail) {
        textViewName?.text = movieDetail.title
        textViewGenres?.text = getGenresName(movieDetail.genres)
        ratingBar?.rating = getRatingValue(movieDetail.voteAverage)
        textViewRating?.text =
            getString(R.string.rating, getRatingValue(movieDetail.voteAverage).toString())
        textViewLanguages?.text =
            getString(R.string.language, getLanguage(movieDetail.spokenLanguages))

        textViewTime?.text = getDateTimeAndRunTime(movieDetail)
        textViewOverview?.text = movieDetail.overview



        imageViewPoster?.let { imageViewPoster ->
            if (movieDetail.posterPath != null) {
                progressDrawable?.let { progress ->
                    loadImage(
                        imageViewPoster,
                        Constant.URL_IMAGE_POSTER + movieDetail.posterPath,
                        progress
                    )
                }
            }
        }
    }

    private fun getDateTimeAndRunTime(movieDetail: MovieDetail): String {
        val date = movieDetail.releaseDate
        val countries =
            movieDetail.productionCountries.joinToString(", ") { item -> item.iso31661.toString() }
        val time = minutesToHour(movieDetail.runtime)


        return "$date ($countries) $time"
    }

    override fun observeLiveData() {
        super.observeLiveData()
        movieDetailViewModel.movieDetail.observe(this, { movieDetail ->
            showMovieDetail(movieDetail)
        })

        movieDetailViewModel.castResponses.observe(this, { resources ->
            when (resources) {
                is State.Success -> {
                    hideLoading()
                    if (resources.data != null) {
                        if (resources.data.isEmpty()) {
                            hideRecyclerView()
                        } else {
                            castAdapter.setData(resources.data)
                            showRecyclerView()
                        }
                    } else {
                        Log.w(TAG, "#observeLiveData response no data")
                    }
                }
                is State.Error -> {
                    showDiaLog()
                }
                is State.Loading -> {
                    showLoading()
                }
            }
        })
    }

    private val clickReload: (() -> Unit) = {
        movieDetailViewModel.getCast(movieDetail?.id)
    }

    private fun showDiaLog() {
        val dialog = showAlertDialog(
            getString(R.string.load_data_failed),
            getString(R.string.message),
            getString(R.string.reload),
            clickReload
        )
        if (dialog?.isShowing == true) return
        dialog?.show()
    }

    private fun showLoading() {
        progressLoading?.isVisible = true
    }

    private fun hideLoading() {
        progressLoading?.isVisible = false
    }

    private fun showRecyclerView() {
        recyclerViewCast?.isVisible = true
        textViewNoResult?.isVisible = false
    }

    private fun hideRecyclerView() {
        recyclerViewCast?.isVisible = false
        textViewNoResult?.isVisible = true
    }

    private var movieDetail: MovieDetail? = null
    private fun getMovieDetail() {
        args.let {
            movieDetail = args.movieDetail
            if (movieDetail != null) {
                movieDetailViewModel.setMovieDetail(movieDetail!!)
                movieDetailViewModel.getCast(movieDetail?.id)
            } else {
                Log.w(TAG, "#getMovieDetail movieDetail is null")
            }
        }
    }

    private fun handleBackButton() {
        toolbar?.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }

}