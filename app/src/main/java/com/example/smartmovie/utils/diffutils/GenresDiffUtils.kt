package com.example.smartmovie.utils.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.example.smartmovie.data.model.Genres

class GenresDiffUtils(
    private val oldData: List<Genres>,
    private val newData: List<Genres>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size
    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldGenres = oldData.getOrNull(oldItemPosition)
        val newGenres = newData.getOrNull(newItemPosition)

        return if (oldGenres != null && newGenres != null) {
            oldGenres.id == newGenres.id
        } else false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldGenres = oldData.getOrNull(oldItemPosition)
        val newGenres = newData.getOrNull(newItemPosition)

        return if (oldGenres != null && newGenres != null) {
            oldGenres.id == newGenres.id && oldGenres.name == newGenres.name
        } else false
    }
}