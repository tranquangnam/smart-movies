package com.example.smartmovie.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmovie.R
import com.example.smartmovie.data.model.Category
import com.example.smartmovie.data.model.MovieDetail
import com.example.smartmovie.utils.Constant.NUMBER_COLUMN_GRID
import com.example.smartmovie.utils.diffutils.CategoryDiffUtils

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private val category = mutableListOf<Category>()
    var onClick: ((position: Int) -> Unit)? = null
    var onMovieClick: ((movieDetail: MovieDetail) -> Unit)? = null


    fun setData(category: List<Category>) {
        try {
            val diffUtil =
                DiffUtil.calculateDiff(CategoryDiffUtils(this.category, category))
            diffUtil.dispatchUpdatesTo(this)

        } catch (ex: Exception) {
        }
        this.category.clear()
        this.category.addAll(category)
    }

    private var isViewType = true
    fun setViewType(type: Boolean) {
        isViewType = type
    }

    class CategoryViewHolder(
        view: View,
        private val onMovieClick: ((movieDetail: MovieDetail) -> Unit)?,
        btnClick: ((position: Int) -> Unit)?,
    ) : RecyclerView.ViewHolder(view) {
        private val linearLayoutManager = LinearLayoutManager(view.context)
        private val gridLayoutManager = GridLayoutManager(view.context, NUMBER_COLUMN_GRID)
        private val tvName: TextView = view.findViewById(R.id.tvName)
        private val btnSeeAll: TextView = view.findViewById(R.id.btnSeeAll)
        private val recyclerViewMovie: RecyclerView = view.findViewById(R.id.rvResult)
        private val movieAdapter = MovieAdapter()
        var viewType: Boolean = true

        init {
            btnSeeAll.setOnClickListener {
                btnClick?.invoke(absoluteAdapterPosition)
            }
        }

        fun onBind(category: Category) {
            tvName.text = category.categoryName
            movieAdapter.setData(category.movies)
            movieAdapter.setViewType(viewType)
            movieAdapter.onClick = { movieDetail ->
                onMovieClick?.invoke(movieDetail)
            }
            recyclerViewMovie.apply {
                layoutManager = if (viewType) {
                    linearLayoutManager
                } else {
                    gridLayoutManager
                }
                adapter = movieAdapter
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return CategoryViewHolder(view, onMovieClick, onClick)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = category[position]
        holder.viewType = isViewType
        holder.onBind(category)
    }

    override fun getItemCount(): Int = category.size
}