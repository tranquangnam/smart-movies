package com.example.smartmovie.ui.fragment.home


import android.view.View
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.smartmovie.R
import com.example.smartmovie.ui.base.BaseFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment() {
    private lateinit var bottomBar: BottomNavigationView
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController
    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun onViewReady(view: View) {
        bottomBar = view.findViewById(R.id.bottomBar)
        navHostFragment =
            childFragmentManager.findFragmentById(R.id.fmHostBottom) as NavHostFragment
        navController = navHostFragment.navController
        bottomBar.setupWithNavController(navController)
    }
}