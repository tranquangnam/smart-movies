package com.example.smartmovie.repository

import androidx.paging.*
import com.example.smartmovie.data.model.*
import com.example.smartmovie.network.ApiService
import com.example.smartmovie.utils.Constant
import com.example.smartmovie.utils.NetworkResponse
import com.example.smartmovie.utils.TypeGet
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepository @Inject constructor(
    private val service: ApiService,
    private val dispatcher: CoroutineDispatcher,
) : IMovieRepository {

    override suspend fun getMovieDetail(movieId: Int): MovieDetail? {
        return withContext(dispatcher) {
            try {
                val response = service.getMovieDetail(movieId)
                val body = response.body()

                if (response.isSuccessful) {
                    body
                } else {
                    null
                }
            } catch (io: IOException) {
                null
            } catch (http: HttpException) {
                null
            }
        }
    }

    override suspend fun getListMovieDetail(movieIds: List<Int?>): List<MovieDetail> {
        return withContext(dispatcher) {
            val movieDetails = mutableListOf<MovieDetail>()
            coroutineScope {
                movieIds.forEach { movieId ->
                    if (movieId != null) {
                        launch {
                            val movieDetail = getMovieDetail(movieId)
                            if (movieDetail != null) {
                                movieDetails.add(movieDetail)
                            }
                            delay(Constant.DELAY_SEARCH_TIME)
                        }
                    }
                }
            }
            return@withContext movieDetails
        }
    }

    override suspend fun getGenres(): NetworkResponse<List<Genres>> {
        return withContext(dispatcher) {
            try {
                val response = service.getGenres()
                val body = response.body()
                val code = response.code()

                if (response.isSuccessful) {
                    val genres = body?.genres
                    if (genres != null) {
                        return@withContext NetworkResponse.Success(genres)
                    } else {
                        return@withContext NetworkResponse.UnknownError(null)
                    }
                } else {
                    return@withContext NetworkResponse.ApiError(code)
                }
            } catch (io: IOException) {
                return@withContext NetworkResponse.NetworkError(io)
            } catch (http: HttpException) {
                return@withContext NetworkResponse.NetworkError(http)
            }
        }
    }

    override suspend fun getCast(movieId: Int): NetworkResponse<List<Cast>> {
        return withContext(dispatcher) {
            try {
                val response = service.getCast(movieId)
                val body = response.body()
                val code = response.code()
                if (response.isSuccessful) {
                    val casts = body?.cast
                    if (casts != null) {
                        return@withContext NetworkResponse.Success(casts)
                    } else {
                        return@withContext NetworkResponse.UnknownError(null)
                    }
                } else {
                    return@withContext NetworkResponse.ApiError(code)
                }
            } catch (io: IOException) {
                return@withContext NetworkResponse.NetworkError(io)
            } catch (http: HttpException) {
                return@withContext NetworkResponse.NetworkError(http)
            }
        }
    }

    override suspend fun querySearch(
        stringQuery: String,
        page: Int?
    ): NetworkResponse<MovieResponseApp> {
        return withContext(dispatcher) {
            try {
                val response = service.search(stringQuery, page)
                val body = response.body()
                val code = response.code()

                if (response.isSuccessful) {
                    if (body != null) {
                        val movieModels = body.results
                        val pageCurrent = body.page
                        val totalPage = body.totalPages

                        if (movieModels != null && pageCurrent != null && totalPage != null) {
                            val movieDetails =
                                getListMovieDetail(movieModels.map { movie -> movie.id }).toMutableList()
                            val movieResponse = MovieResponseApp(
                                Constant.STRING_EMPTY,
                                pageCurrent,
                                movieDetails,
                                totalPage
                            )
                            NetworkResponse.Success(movieResponse)
                        } else {
                            NetworkResponse.UnknownError(null)
                        }
                    } else {
                        NetworkResponse.UnknownError(null)
                    }
                } else {
                    NetworkResponse.ApiError(code)
                }
            } catch (io: IOException) {
                NetworkResponse.NetworkError(io)
            } catch (http: HttpException) {
                NetworkResponse.NetworkError(http)
            }
        }
    }

    override suspend fun getMovie(page: Int, type: TypeGet): NetworkResponse<MovieResponseApp> {
        return withContext(dispatcher) {
            try {
                var name: String? = null
                val response = when (type) {
                    TypeGet.POPULAR -> {
                        name = Constant.POPULAR_NAME
                        service.getMoviePopular(page)
                    }
                    TypeGet.UP_COMING -> {
                        name = Constant.UP_COMING_NAME
                        service.getMovieUpComing(page)
                    }
                    TypeGet.NOW_PLAYING -> {
                        name = Constant.NOW_PLAYING_NAME
                        service.getMovieNowPlaying(page)
                    }
                    TypeGet.TOP_RATED -> {
                        name = Constant.TOP_RATED_NAME
                        service.getMovieTopRated(page)
                    }
                }
                val body = response.body()
                val code = response.code()

                if (response.isSuccessful) {
                    if (body != null) {
                        val movieModels = body.results
                        val pageCurrent = body.page
                        val totalPage = body.totalPages

                        if (movieModels != null && pageCurrent != null && totalPage != null) {
                            val movieDetails =
                                getListMovieDetail(movieModels.map { movie -> movie.id }).toMutableList()
                            val movieResponse = MovieResponseApp(
                                name,
                                pageCurrent,
                                movieDetails,
                                totalPage
                            )
                            NetworkResponse.Success(movieResponse)
                        } else {
                            NetworkResponse.UnknownError(null)
                        }
                    } else {
                        NetworkResponse.UnknownError(null)
                    }
                } else {
                    NetworkResponse.ApiError(code)
                }
            } catch (io: IOException) {
                NetworkResponse.NetworkError(io)
            } catch (http: HttpException) {
                NetworkResponse.NetworkError(http)
            }
        }
    }

    override suspend fun getMovieFromGenres(
        genresId: Int,
        page: Int
    ): NetworkResponse<MovieResponseApp> {
        return withContext(dispatcher) {
            try {
                val response = service.getMovieFromGenres(genresId, page)
                val body = response.body()
                val code = response.code()

                if (response.isSuccessful) {
                    if (body != null) {
                        val movieModels = body.results
                        val pageCurrent = body.page
                        val totalPage = body.totalPages

                        if (movieModels != null && pageCurrent != null && totalPage != null) {
                            val movieDetails =
                                getListMovieDetail(movieModels.map { movie -> movie.id }).toMutableList()
                            val movieResponse = MovieResponseApp(
                                Constant.STRING_EMPTY,
                                pageCurrent,
                                movieDetails,
                                totalPage
                            )
                            NetworkResponse.Success(movieResponse)
                        } else {
                            NetworkResponse.UnknownError(null)
                        }
                    } else {
                        NetworkResponse.UnknownError(null)
                    }
                } else {
                    NetworkResponse.ApiError(code)
                }
            } catch (io: IOException) {
                NetworkResponse.NetworkError(io)
            } catch (http: HttpException) {
                NetworkResponse.NetworkError(http)
            }
        }
    }
}

